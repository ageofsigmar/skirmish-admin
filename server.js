var config = require('./libs/config');
var express = require('express');
var path = require('path'); // модуль для парсинга пути
var favicon = require('serve-favicon');
var morgan = require('morgan');
var methodOverride = require('method-override')
var bodyParser = require('body-parser');
var log = require('./libs/log')(module);
var UnitModel = require('./libs/mongoose').UnitModel;
var WarbandModel = require('./libs/mongoose').WarbandModel;
var AliancesModel = require('./libs/mongoose').AliancesModel;
var BattleTraitsModel = require('./libs/mongoose').BattleTraitsModel;
var ArtefactsModel = require('./libs/mongoose').ArtefactsModel;
var CommandTraitsModel = require('./libs/mongoose').CommandTraitsModel;
var CommandAbilitiesModel = require('./libs/mongoose').CommandAbilitiesModel;
var SpellsModel = require('./libs/mongoose').SpellsModel;
var UserModel = require('./libs/mongoose').UserModel;
var app = express();
var cors = require('cors')
var jwt  = require('jsonwebtoken');

app.set('superSecret', 'ilovescotchyscotch'); // secret variable
app.use(cors());

app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(morgan('tiny')); // выводим все запросы со статусами в консоль
app.use(bodyParser.json()); // стандартный модуль, для парсинга JSON в запросах
app.use(methodOverride('X-HTTP-Method-Override')); // поддержка put и delete
app.use(express.static(path.join(__dirname, "public"))); // запуск статического файлового сервера, который смотрит на папку public/ (в нашем случае отдает index.html)

app.get('/api', function (req, res) {
    res.send('API is running');
});

app.listen(config.get('port'), function(){
    log.info('Express server listening on port ' + config.get('port'));
});

app.post('/authenticate', function(req, res) {
    UserModel.findOne({
        name: req.body.name
    },
    function(err, user) {
        if (err) {
            throw err;
        }
        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {
            if (user.password != req.body.password) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
            } else {
                var token = jwt.sign(user, app.get('superSecret'), {
                    expiresIn : 1440
                });
                res.json({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token
                });
            }
        }
    });
});


app.use(function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});


app.get('/api/skirmish', function(req, res) {
    return AliancesModel.find({}, function (err, aliances) {
        if (!err) {
            return res.send({ status: 'OK', aliances:aliances });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/skirmish', function(req, res) {
    var aliance = new AliancesModel({
        _id: (req.body._id).trim(),
        description: req.body.description
    });

    aliance.save(function (err) {
        if (!err) {
            log.info("aliance created");
            return res.send({ status: 'OK', aliance:aliance });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

app.put('/api/skirmish', function(req, res) {
    return AliancesModel.findById((req.body._id).trim(), function (err, aliance) {
        if(!aliance) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            aliance._id = req.body._id;
            aliance.description = req.body.description;
            aliance.save(function () {
                return res.send({ status: 'OK', aliance:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/skirmish', function(req, res) {
    return AliancesModel.remove({ _id: req.query._id }, function (err, aliance) {
        if(!aliance) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', aliance:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance', function(req, res) {
    return WarbandModel.find( {'aliance': req.params.aliance}, function (err, warbands) {
        if(!warbands) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', warbands:warbands });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/skirmish/:aliance', function(req, res) {
    var warband = new WarbandModel({
        _id: (req.body._id).trim(),
        description: req.body.description,
        aliance: req.params.aliance
    });

    warband.save(function (err) {
        if (!err) {
            log.info("aliance created");
            return res.send({ status: 'OK', warband:warband });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

app.put('/api/skirmish/:aliance', function(req, res) {
    return WarbandModel.findById((req.body._id).trim(), function (err, warband) {
        if(!warband) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            warband._id = req.body._id;
            warband.description = req.body.description;
            warband.aliance = req.body.aliance;
            warband.save(function () {
                return res.send({ status: 'OK', warband:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/skirmish/:aliance', function(req, res) {
    return WarbandModel.remove({ _id: req.query._id }, function (err, warband) {
        if(!warband) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', warband:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance/units', function(req, res) {
    return UnitModel.find( {}, function (err, units) {
        if(!units) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', units:units });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance/units/:warband', function(req, res) {
    return UnitModel.find( {'warband': req.params.warband}, function (err, units) {
        if(!units) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', units:units });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});


app.post('/api/skirmish/:aliance/units/:warband', function(req, res) {
    var unit = new UnitModel({
        _id: (req.body._id).trim(),
        warband: req.params.warband,
        story: req.body.story,
        scroll_move: req.body.scroll_move,
        scroll_wounds: req.body.scroll_wounds,
        scroll_save: req.body.scroll_save,
        scroll_bravery: req.body.scroll_bravery,
        missile_weapons: req.body.missile_weapons,
        melee_weapons: req.body.melee_weapons,
        description: req.body.description,
        wargear: req.body.wargear,
        abilities: req.body.abilities,
        spells: req.body.spells,
        command_abilities: req.body.command_abilities,
        keywords: req.body.keywords,
        hero: req.body.hero,
        mage: req.body.mage,
        magic_lvl: req.body.magic_lvl,
        image: req.body.image,
        min: req.body.min,
        max: req.body.max,
        cost: req.body.cost,
        skirmish: req.body.skirmish,
        skirmish_hero: req.body.skirmish_hero,
        skirmish_official: req.body.skirmish_official,
        skirmish_min: req.body.skirmish_min,
        skirmish_max: req.body.skirmish_max,
        skirmish_cost: req.body.skirmish_cost,
        monster: req.body.monster,
        priest: req.body.priest,
        war_machine: req.body.war_machine,
        totem: req.body.totem,
        engineer: req.body.engineer,
        damage_table: req.body.damage_table

});

    unit.save(function (err) {
        if (!err) {
            log.info("unit created");
            return res.send({ status: 'OK', unit:unit });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

app.put('/api/skirmish/:aliance/units/:warband', function(req, res) {
    return UnitModel.findById((req.body._id).trim(), function (err, unit) {
        if(!unit) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            unit._id = req.body._id;
            unit.warband = req.body.warband;
            unit.story = req.body.story;
            unit.scroll_move = req.body.scroll_move;
            unit.scroll_wounds = req.body.scroll_wounds;
            unit.scroll_save = req.body.scroll_save;
            unit.scroll_bravery = req.body.scroll_bravery;
            unit.missile_weapons = req.body.missile_weapons;
            unit.melee_weapons = req.body.melee_weapons;
            unit.description = req.body.description;
            unit.wargear = req.body.wargear;
            unit.abilities = req.body.abilities;
            unit.spells = req.body.spells;
            unit.command_abilities = req.body.command_abilities;
            unit.keywords = req.body.keywords;
            unit.hero = req.body.hero;
            unit.mage = req.body.mage;
            unit.magic_lvl = req.body.magic_lvl;
            unit.image = req.body.image;
            unit.min = req.body.min;
            unit.max = req.body.max;
            unit.cost = req.body.cost;
            unit.skirmish = req.body.skirmish;
            unit.skirmish_hero = req.body.skirmish_hero;
            unit.skirmish_official = req.body.skirmish_official;
            unit.skirmish_min = req.body.skirmish_min;
            unit.skirmish_max = req.body.skirmish_max;
            unit.skirmish_cost = req.body.skirmish_cost;
            unit.monster = req.body.monster;
            unit.priest = req.body.priest;
            unit.war_machine = req.body.war_machine;
            unit.totem = req.body.totem;
            unit.engineer = req.body.engineer;
            unit.damage_table = req.body.damage_table;

            unit.save(function () {
                return res.send({ status: 'OK', unit:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/skirmish/:aliance/units/:warband', function(req, res) {
    return UnitModel.remove({ _id: req.query._id }, function (err, unit) {
        if(!unit) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', unit:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance/heroes', function(req, res) {
    return UnitModel.find( {'skirmish_hero': true}, function (err, heroes) {
        if(!heroes) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', heroes:heroes });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance/heroes/:warband', function(req, res) {
    return UnitModel.find( {'skirmish_hero': true, 'warband': req.params.warband}, function (err, heroes) {
        if(!heroes) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', heroes:heroes });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance/artefacts', function(req, res) {
    return ArtefactsModel.find( {'aliance': req.params.aliance}, function (err, artefacts) {
        if(!artefacts) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', artefacts:artefacts });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/skirmish/:aliance/artefacts', function(req, res) {
    var artefact = new ArtefactsModel({
        _id: (req.body._id).trim(),
        description: req.body.description,
        aliance: req.params.aliance
    });

    artefact.save(function (err) {
        if (!err) {
            log.info("trait created");
            return res.send({ status: 'OK', artefact:artefact });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});


app.put('/api/skirmish/:aliance/artefacts', function(req, res) {
    return ArtefactsModel.findById((req.body._id).trim(), function (err, artefact) {
        if(!artefact) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            artefact._id = req.body._id;
            artefact.description = req.body.description;
            artefact.aliance = req.body.aliance;
            artefact.save(function () {
                return res.send({ status: 'OK', artefact:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/skirmish/:aliance/artefacts', function(req, res) {
    return ArtefactsModel.remove({ _id: req.query._id }, function (err, artefact) {
        if(!artefact) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', artefact:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance/traits/battle', function(req, res) {
    return BattleTraitsModel.find( {'aliance': req.params.aliance}, function (err, traits) {
        if(!traits) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', traits:traits });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/skirmish/:aliance/traits/battle', function(req, res) {
    var trait = new BattleTraitsModel({
        _id: (req.body._id).trim(),
        description: req.body.description,
        aliance: req.params.aliance
    });

    trait.save(function (err) {
        if (!err) {
            log.info("trait created");
            return res.send({ status: 'OK', trait:trait });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});


app.put('/api/skirmish/:aliance/traits/battle', function(req, res) {
    return BattleTraitsModel.findById((req.body._id).trim(), function (err, trait) {
        if(!trait) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            trait._id = req.body._id;
            trait.description = req.body.description;
            trait.aliance = req.body.aliance;
            trait.save(function () {
                return res.send({ status: 'OK', trait:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/skirmish/:aliance/traits/battle', function(req, res) {
    return BattleTraitsModel.remove({ _id: req.query._id }, function (err, trait) {
        if(!trait) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', trait:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/skirmish/:aliance/traits/command', function(req, res) {
    return CommandTraitsModel.find( {'aliance': req.params.aliance}, function (err, traits) {
        if(!traits) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', traits:traits });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/skirmish/:aliance/traits/command', function(req, res) {
    var trait = new CommandTraitsModel({
        _id: (req.body._id).trim(),
        description: req.body.description,
        aliance: req.params.aliance
    });

    trait.save(function (err) {
        if (!err) {
            log.info("trait created");
            return res.send({ status: 'OK', trait:trait });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

app.put('/api/skirmish/:aliance/traits/command', function(req, res) {
    return CommandTraitsModel.findById((req.body._id).trim(), function (err, trait) {
        if(!trait) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            trait._id = req.body._id;
            trait.description = req.body.description;
            trait.aliance = req.body.aliance;
            trait.save(function () {
                return res.send({ status: 'OK', trait:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/skirmish/:aliance/traits/command', function(req, res) {
    return CommandTraitsModel.remove({ _id: req.query._id }, function (err, trait) {
        if(!trait) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', trait:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/spells', function(req, res) {
    return SpellsModel.find( {}, function (err, spells) {
        if(!spells) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', spells:spells });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/spells', function(req, res) {
    var spell = new SpellsModel({
        _id: (req.body._id).trim(),
        description: req.body.description
    });

    spell.save(function (err) {
        if (!err) {
            log.info("spell created");
            return res.send({ status: 'OK', spell:spell });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

app.put('/api/spells', function(req, res) {
    return SpellsModel.findById((req.body._id).trim(), function (err, spell) {
        if(!spell) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            spell._id = req.body._id;
            spell.description = req.body.description;
            spell.save(function () {
                return res.send({ status: 'OK', spell:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/spells', function(req, res) {
    return SpellsModel.remove({ _id: req.query._id }, function (err, spell) {
        if(!spell) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', spell:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/abilities/command', function(req, res) {
    return CommandAbilitiesModel.find( {}, function (err, abilities) {
        if(!abilities) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', abilities:abilities });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/abilities/command', function(req, res) {
    var ability = new CommandAbilitiesModel({
        _id: (req.body._id).trim(),
        description: req.body.description
    });

    ability.save(function (err) {
        if (!err) {
            log.info("spell created");
            return res.send({ status: 'OK', ability:ability });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 200;
                res.send({error: 'Validation error'});
            } else if(err.code == 11000) {
                res.statusCode = 200;
                res.send({error: 'Duplicate entry'});
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

app.put('/api/abilities/command', function(req, res) {
    return CommandAbilitiesModel.findById((req.body._id).trim(), function (err, ability) {
        if(!ability) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            ability._id = req.body._id;
            ability.description = req.body.description;
            ability.save(function () {
                return res.send({ status: 'OK', ability:{_id: req.body._id, message: "successfully updated"} });
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.delete('/api/abilities/command', function(req, res) {
    return CommandAbilitiesModel.remove({ _id: req.query._id }, function (err, ability) {
        if(!ability) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', ability:{_id: req.body._id, message: "successfully deleted"} });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.get('/api/generals/:aliance', function(req, res) {
    return WarbandModel.find( {'parent': req.params.aliance}, function (err, warbands) {
         if(!warbands) {
             res.statusCode = 404;
             return res.send({ error: 'Not found' });
         }
         if (!err) {
             if(typeof warbands === "object" && Object.keys(warbands).length > 0) {
                 var array = [];

                 Object.keys(warbands).map(function (p1, p2, p3) {
                     array.push(warbands[p1]._id);
                 });

                 return UnitModel.find( {parent: { $in : array }, 'type': 'Unit', 'hero': 'true'}, function (err, unit) {
                     if(!unit) {
                         res.statusCode = 404;
                         return res.send({ error: 'Not found' });
                     }
                     if (!err) {
                         return res.send({ status: 'OK', generals:unit });
                     }
                     else {
                         res.statusCode = 500;
                         log.error('Internal error(%d): %s',res.statusCode,err.message);
                         return res.send({ error: 'Server error' });
                     }
                 });
             } else {
                 res.statusCode = 404;
                 return res.send({ error: 'Not found' });
             }
         } else {
             res.statusCode = 500;
             log.error('Internal error(%d): %s',res.statusCode,err.message);
             return res.send({ error: 'Server error' });
         }
     });
});

app.put('/api/units/:id', function (req, res){
    return UnitModel.findById(req.params.id, function (err, unit) {
        if(!unit) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        unit._id = req.body._id;
        unit.parent = req.body.parent;
        unit.type = req.body.type;
        unit.official = req.body.official;
        unit.min = req.body.min;
        unit.max = req.body.max;
        unit.cost = req.body.cost;
        unit.hero = req.body.hero;

        return unit.save(function (err) {
            if (!err) {
                log.info("unit updated");
                return res.send({ status: 'OK', unit:unit });
            } else {
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
            }
        });
    });
});

app.delete('/api/units/:id', function (req, res){
    return UnitModel.findById(req.params.id, function (err, unit) {
        if(!unit) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        return unit.remove(function (err) {
            if (!err) {
                log.info("unit removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    });
});

app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});