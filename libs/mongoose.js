var mongoose    = require('mongoose');
var log         = require('./log')(module);
var config      = require('./config');


var mUnit      = require('../models/unit').unit;
var mWarband   = require('../models/warband').warband;
var mAliance   = require('../models/aliance').aliance;

var mCTraits        = require('../models/cTraits').cTraits;
var mCAbilities     = require('../models/cAbilities').cAbilities;
var mArtefacts      = require('../models/artefacts').artefacts;
var mBattleTraits   = require('../models/bTraits').bTraits;
var mSpells         = require('../models/spells').spell;
var user   = require('../models/user').user; // get our mongoose model

mongoose.connect(config.get('mongoose:uri'));
var db = mongoose.connection;

db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback () {
    log.info("Connected to DB!");
});

var Schema = mongoose.Schema;

var Unit = new Schema(mUnit, { collection: 'Units' });
Unit.path('_id').validate(function (v) {
    return v.length > 5 && v.length < 70;
});
var UnitModel = mongoose.model('Unit', Unit);

var User = new Schema(user, { collection: 'Users' });
var UserModel = mongoose.model('User', User);

var Warband = new Schema(mWarband, { collection: 'Warbands' });
var WarbandModel = mongoose.model('Warband', Warband);

var Aliances = new Schema(mAliance, { collection: 'Grand Aliances' });
var AliancesModel = mongoose.model('Aliances', Aliances);

var commandTraits = new Schema(mCTraits, { collection: 'Command Traits' });
var CommandTraitsModel = mongoose.model('commandTraits', commandTraits);

var commandAbilities = new Schema(mCAbilities, { collection: 'Command Abilities' });
var CommandAbilitiesModel = mongoose.model('commandAbilities', commandAbilities);

var artefacts = new Schema(mArtefacts, { collection: 'Artefacts' });
var ArtefactsModel = mongoose.model('artefacts', artefacts);

var battleTraits = new Schema(mBattleTraits, { collection: 'Battle Traits' });
var BattleTraitsModel = mongoose.model('battleTraits', battleTraits);

var Spells = new Schema(mSpells, { collection: 'Spells' });
var SpellsModel = mongoose.model('Spells', Spells);

module.exports.UserModel = UserModel;
module.exports.UnitModel = UnitModel;
module.exports.WarbandModel = WarbandModel;
module.exports.AliancesModel = AliancesModel;
module.exports.SpellsModel = SpellsModel;
module.exports.BattleTraitsModel = BattleTraitsModel;
module.exports.ArtefactsModel = ArtefactsModel;
module.exports.CommandAbilitiesModel = CommandAbilitiesModel;
module.exports.CommandTraitsModel = CommandTraitsModel;