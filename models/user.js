var user  = {
    name: { type: String, required: true },
    password: { type: String, required: true },
    admin: { type: Boolean , required: false }
};

module.exports.user = user;