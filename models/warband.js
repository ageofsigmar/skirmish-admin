var warband  = {
    _id: { type: String, required: true },
    description: { type: String, required: false },
    aliance: { type: String, required: true }
};

module.exports.warband = warband;