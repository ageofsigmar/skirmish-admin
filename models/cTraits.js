var cTraits  = {
    _id: { type: String, required: true },
    aliance: { type: String, required: true },
    description: { type: String, required: false }
};

module.exports.cTraits = cTraits;