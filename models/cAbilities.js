var cAbilities  = {
    _id: { type: String, required: true },
    description: { type: String, required: false }
};

module.exports.cAbilities = cAbilities;